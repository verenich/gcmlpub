import torch
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F



# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
# setup params
num_epochs = 80
cam_activation_point = 0.001
# this depends on the final resolution map size of the CNN, and is just h*w of it.
gcl_length = 16

# Image preprocessing modules
transform = transforms.Compose([
    transforms.Pad(4),
    #transforms.RandomHorizontalFlip(),
    transforms.RandomCrop(32),
    transforms.ToTensor()])

# CIFAR-10 dataset, this will automatically download the first time it's ran
train_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                             train=True, 
                                             transform=transform,
                                             download=True)

test_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                            train=False, 
                                            transform=transforms.ToTensor())

# Data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=200, 
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=10, 
                                          shuffle=False)


class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()

def compute_cams(activations, labels, weights):
    # get number of batches
    cams = []
    gcl_index = []
    batches,_,_,_ = activations.size()
    for b in range(batches):
        cw = weights[labels[b],:] # get class weights for label[b]
        a = activations[b,:]
        # dot product between class weights vector and filter activations
        cam = (cw.view(-1,1,1) * a).sum(0)
        cam = F.relu(cam)
        # normalize
        cam_min, cam_max = cam.min(), cam.max()
        cam = (cam - cam_min).div(cam_max - cam_min)
        cams.append(cam)
        # print('CAM:\n',cam)
        # n,m = cam.size()
        index = cam_to_index(cam.view(-1), gcl_length)
        gcl_index.append(index.item())
        #print("Index: ", index.item())

    return cams, gcl_index


# takes a 2D tensor and computes an index for global correlation vector,
# which is a powerset of all positive CAM activations
def cam_to_index(cam, bits):
    # cam_activation_point is the threshhold on cam activations
    a = torch.where(cam > cam_activation_point, 1, 0)
    b = a.view(-1)
    mask = 2 ** torch.arange(bits - 1, -1, -1).to(b.device, b.dtype)
    return torch.sum(mask * b, -1)



if __name__ == '__main__':

    # create the GCL structure, we use ones for Laplace smoothing
    gcl = torch.ones(10, (1 << gcl_length))
    gcl = gcl.to(device)
    # LOAD OUR FEATURE EXTRACTOR
    model = torch.load('models/res4cifNoFlip.pt')
    model.eval()
    model = model.to(device)
    #print(model)
    hook = Hook(model.layer4[1].bn2)
    print("Begin GCML training....")
    with torch.no_grad():
        for epoch in range(num_epochs):
        
            for i, (images, labels) in enumerate(train_loader):
                images = images.to(device)
                labels = labels.to(device)
                outputs = model(images)
                # print("HOOK ACTIVATIONS: ", hook.output.size())
                # print("Labels: ", labels)
                cams, gcls = compute_cams(hook.output, labels, model.fc.weight)
                #print("GCL: ", gcls)

                for (label, index) in zip(labels, gcls):
                    gcl[label][index] += 1.0
            print("Epoch complete: ",epoch)
                
        # PERSIST our GCL file, we'll use it to perform predictions inside test_gcml.py
        torch.save(gcl,'gcml/gcl001NoFlip.pt')
    # let's look at the weigths tensor for class 3

    #print("Weights at fc:\n", model.fc.weight[3,:].size())