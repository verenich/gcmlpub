import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np


# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
num_epochs = 1
learning_rate = 0.001
cam_activation_point = 0.01
# this depends on the final resolution map size of the CNN, and is just h*w of it.
gcl_length = 16
# this is for cifar10, should be changed accordingly to reflect data balance or imbalance
priors = torch.tensor([0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1])

#priors = torch.tensor([0.05, 0.05, 0.05, 0.05, 0.3, 0.1, 0.1, 0.1, 0.1, 0.1])


# Image preprocessing modules
transform = transforms.Compose([
    transforms.Pad(4),
    transforms.RandomHorizontalFlip(),
    transforms.RandomCrop(32),
    transforms.ToTensor()])

# CIFAR-10 dataset
train_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                             train=True, 
                                             transform=transforms.ToTensor(),
                                             download=True)

test_dataset = torchvision.datasets.CIFAR10(root='../../data/',
                                            train=False, 
                                            transform=transforms.ToTensor())

# Data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=200, 
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=10, 
                                          shuffle=False)


class Hook():
    def __init__(self, module, backward=False):
        if backward==False:
            self.hook = module.register_forward_hook(self.hook_fn)
        else:
            self.hook = module.register_backward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.input = input
        self.output = output
    def close(self):
        self.hook.remove()

def compute_cams(activations, labels, weights):
    # get number of batches
    cams = []
    gcl_index = []
    batches,_,_,_ = activations.size()
    for b in range(batches):
        cw = weights[labels[b],:] # get class weights for label[b]
        a = activations[b,:]
        # dot product between class weights vector and filter activations
        cam = (cw.view(-1,1,1) * a).sum(0)
        cam = F.relu(cam)
        # normalize
        cam_min, cam_max = cam.min(), cam.max()
        cam = (cam - cam_min).div(cam_max - cam_min)
        cams.append(cam)
        # print('CAM:\n',cam)
        # n,m = cam.size()
        index = cam_to_index(cam.view(-1), gcl_length)
        gcl_index.append(index.item())
        #print("Index: ", index.item())

    return cams, gcl_index


def compute_cam_classes(activations, weights, gcltable):
    # get number of batches
    gcl_index = []
    batches,_,_,_ = activations.size()
    # number of classes in this CNN
    classes, _ = weights.size()
    index_matrix = torch.zeros([batches, classes], dtype=torch.int64)
    probs_matrix = torch.zeros(index_matrix.size(), dtype=torch.float32)
    
    for b in range(batches):
        
        a = activations[b,:]
        # compute cams for each class
        
        for c in range(classes):
            cw = weights[c,:] # get class weights for class c
            # dot product between class weights vector and filter activations
            cam = (cw.view(-1,1,1) * a).sum(0)
            cam = F.relu(cam)
            # normalize
            cam_min, cam_max = cam.min(), cam.max()
            cam = (cam - cam_min).div(cam_max - cam_min)
            index = cam_to_index(cam.view(-1), gcl_length)
            #print("Index: ", index)
            index_matrix[b][c] = index
            # get the likelihood probability
            probs_matrix[b][c] = gcltable[c][index]
    return index_matrix, probs_matrix

# takes a 2D tensor and computes an index for global correlation vector,
# which is a powerset of all positive CAM activations
def cam_to_index(cam, bits):
    # cam_activation_point is the threshhold on cam activations
    a = torch.where(cam > cam_activation_point, 1, 0)
    b = a.view(-1)
    mask = 2 ** torch.arange(bits - 1, -1, -1).to(b.device, b.dtype)
    return torch.sum(mask * b, -1)


def compute_posteriors(predicted, matrix, gcl):
    # number of classes
    _, n_classes = matrix.size()
    # first collect the likelihoods from predicted classes
    lk = torch.zeros(predicted.size()[0], dtype=torch.float32)
    # lk_den = torch.zeros(predicted.size()[0], n_classes, dtype=torch.float32)
    post = torch.zeros(predicted.size()[0], dtype=torch.float32)
    i = 0
    for c in predicted:
        #print("I: ", matrix[i][c])
        # first build the batch likelihood array of size [predicted]
        lk[i] = gcl[c][matrix[i][c]]
        # now we gather likelihoods for all classes along index matrix[i][c]
        lksd = gcl[:,matrix[i][c]]
        den = (lksd * priors).sum(0)
        pp = (lk[i] * priors[c]) / den
        #print("den: \n",lksd)
        post[i] = pp
        i += 1
    print("ML:", lk)
    # print("Posterior vector:", post)
    return post
    # try the gather method
    # matrix = matrix.to(device)
    # indexes = matrix.gather(1, predicted.unsqueeze(1))
    


if __name__ == '__main__':

    # create the GCL structure, we use ones for Laplace smoothing
    gcl = torch.load('gcl01.pt')

    x,_ = gcl.size()
    for y in range(x):
        gcl[y] = gcl[y] / gcl[y].sum(0)    

    gcl = gcl.to(device)
    model = torch.load('models/res4x4.pt')
    model.eval()
    model = model.to(device)
    priors = priors.to(device)
    #print(model)
    hook = Hook(model.layer4[1].bn2)
    counter = 0

    total = 0
    correct = 0
    correct_fl = 0
    with torch.no_grad(): 
        for i, (images, labels) in enumerate(test_loader):
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)

            # correct on cnn preds
            _, pred = torch.max(outputs.data, 1)
            # print("P:", pred)
            # print("L:", labels)
            correct_fl += (pred == labels).sum().item()
            # print("Output: ", outputs)
            # print("Labels: ", labels)

            #matrix = compute_cams(hook.output, labels, model.fc.weight)
            matrix, probs = compute_cam_classes(hook.output, model.fc.weight, gcl)
            print("Matrix Probs:\n",probs)
            values, predicted = probs.max(1)
            predicted = predicted.to(device)
            print("P: ", predicted)
            print("L: ", labels)
            # try likelihood
            post = compute_posteriors(predicted, matrix, gcl)
            print("G:", post)
            # print(labels)
            # compute accuracy
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            counter += 1
            if counter > 2:
                break
         # calculate accuracy
        print('G Accuracy: {} %'.format(100 * correct / total))
        print('C Accuracy: {} %'.format(100 * correct_fl / total))