# Edward Verenich <verenie@clarkson.edu>
# MIT license <https://opensource.org/licenses/MIT>

import torch
import torchvision.transforms as transforms
import torchvision.transforms.functional as F
import cv2
import numpy as np
import matplotlib.pyplot as plt
import itertools
from PIL import Image

# mean and std for imagenet based models
IMAGE_NET_MEAN = [0.485, 0.456, 0.406]
IMAGE_NET_STD = [0.229, 0.224, 0.225]

def open_image(path):
    """Load image from path as a PIL RBB
    Args: 
        path (str): local path to the image file
    Returns:
        Instance of PIL.Image.Image in RGB
    """
    return Image.open(path).convert('RGB')

def transform_to_tensor(image, size=224):
    # check image is a PIL
    if not isinstance(image, Image.Image):
        image = F.to_pil_image(image)
    # apply transforms
    transform = transforms.Compose([
        transforms.Resize(size),
        transforms.CenterCrop(size),
        transforms.ToTensor(),
        transforms.Normalize(IMAGE_NET_MEAN,IMAGE_NET_STD)

    ])
    tensor = transform(image).unsqueeze(0)
    tensor.requires_grad_()

    return tensor



def normalize(tensor, mean, std):
    # check for 4 dimensions including minibatch
    if tensor.ndimension() != 4:
        raise TypeError('tensors needs 4 dimensions, got ', tensor.ndimension())
    mean = torch.FloatTensor(mean).view(1,3,1,1).expand_as(tensor).to(tensor.device)
    std = torch.FloatTensor(std).view(1,3,1,1).expand_as(tensor).to(tensor.device)
    return tensor.sub(mean).div(std)

def denormalize(tensor, mean, std):
    # check for 4 dimensions including minibatch
    if tensor.ndimension() != 4:
        raise TypeError('tensors needs 4 dimensions, got ', tensor.ndimension())
    mean = torch.FloatTensor(mean).view(1,3,1,1).expand_as(tensor).to(tensor.device)
    std = torch.FloatTensor(std).view(1,3,1,1).expand_as(tensor).to(tensor.device)
    return tensor.mul(std).add(mean)

def fuse_cam(img, mask):
    """Combines the input image and a scaled cam to produce an overlay of image and heatmap.
    Args: 
        img (torch.tensor): img shape (1, 3, H, W) where each entry is in range [0, 1]
        mask (torch.tensor): mask shape (1, 1, H, W) where each entry is in range [0, 1]
    
    Return:
        heatmap (torch.tensor): heatmap img shape of (3, H, W)
        result (torch.tensor): fused Class Activation Map result of same shape with heatmap.
    """
    heatmap = cv2.applyColorMap(np.uint8(255 * mask.squeeze()), cv2.COLORMAP_JET)
    heatmap = torch.from_numpy(heatmap).permute(2, 0, 1).float().div(255)
    b, g, r = heatmap.split(1)
    heatmap = torch.cat([r, g, b])
    
    result = heatmap+img.cpu()
    result = result.div(result.max()).squeeze()
    
    return heatmap, result

def show_plot(samples):
    n = len(samples)
    f, axarr = plt.subplots(1,n)
    i = 0
    for sample in samples:
        inp = sample.detach().numpy().transpose((1, 2, 0))
        mean = np.array(IMAGE_NET_MEAN)
        std = np.array(IMAGE_NET_STD)
        inp = std * inp + mean
        inp = np.clip(inp, 0, 1)

        axarr[i].imshow(inp)
        i +=1
    plt.pause(0.001)
    plt.show()

def show_overlays(image, mask):

    f, axarr = plt.subplots()
    # image is expected to by in cpu memory
    inp = torch.squeeze(image, 0)
    inp = inp.detach().numpy().transpose((1, 2, 0))
    mean = np.array(IMAGE_NET_MEAN)
    std = np.array(IMAGE_NET_STD)
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)

    axarr.imshow(inp)
    # overlay mask
    mask = torch.squeeze(mask, 0)
    mask = mask.detach().numpy().transpose((1,2,0))
    mask = np.clip(mask, 0, 1)
    axarr.imshow(mask, cmap='jet', alpha=0.5)

    plt.pause(0.001)
    plt.show()



def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    # increase default font size
    plt.rcParams.update({'font.size': 15})
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    # increase default font size
    #plt.rcParams.update({'font.size': 15})
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    # plt.ylabel('True label')
    # plt.xlabel('Predicted label')
    plt.ylabel('Predicted label')
    plt.xlabel('True label')

    plt.show()

if __name__ == '__main__':

    # test our util functions
    # pil = open_image('data/car6.jpeg')
    # image = transform_to_tensor(pil, size=224)

    # create a random mask
    # mask = torch.normal(0,1,size=(224,224))
    # mask = torch.nn.functional.relu(mask)
    # samples = []
    # try a resnet model
    

    # # model args
    # model_dict = dict(model_type='resnet', arch=resnet, layer_name='layer4', input_size=(224,224))
    # gc = GradCAMpp(model_dict, verbose=True)
    # # get the cam
    # mask, logit = gc(image, class_idx=479)

    # fuse
    # heat, res = fuse_cam(image, mask)
    # samples.append(heat)
    # samples.append(res)
    # # plot
    # show_plot(samples)
    # UL-hopfield cifar confusion matrix AS REPORTED IN THEIR PAPER
    class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
    cm = torch.tensor([
        [828,13,12,11,18,0,2,4,85,27],
        [10,910,0,5,1,1,0,1,11,61],
        [47,1,708,64,88,14,63,4,8,3],
        [3,4,16,768,33,93,50,19,4,10],
        [10,0,39,43,788,12,57,43,6,2],
        [2,0,10,137,29,777,8,33,0,4],
        [7,2,10,54,29,7,888,1,1,1],
        [24,2,14,39,76,17,4,818,2,4],
        [27,13,0,7,3,0,3,0,933,14],
        [19,64,1,7,2,1,1,0,18,887]
        ])

    plot_confusion_matrix(cm, class_names)
    
